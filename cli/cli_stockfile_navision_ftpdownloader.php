<?php
/**
 * src/StockFileNavisionFTPDownloader.php
 *
 * @author mrx <mma@shopall24.com>
 * 
 * @package DataImportStockFileNavisionFTPDownloader
 * @subpackage StockFileNavisionFTPDownloader
 * @version 1.0.0
 */

// Free spech https://github.com/Behat/Behat/blob/master/bin/behat
if (is_file($autoload = getcwd() . '/vendor/autoload.php')) {
    require $autoload;
}

if (is_file($autoload = __DIR__ . '/../vendor/autoload.php')) {
    require($autoload);
} elseif (is_file($autoload = __DIR__ . '/../../../autoload.php')) {
    require($autoload);
} else {
    fwrite(STDERR,
        'You must set up the project dependencies, run the following commands:'.PHP_EOL.
        'curl -s http://getcomposer.org/installer | php'.PHP_EOL.
        'php composer.phar install'.PHP_EOL
    );
    exit(1);
}

$dotenv = new \Dotenv\Dotenv( dirname( __DIR__ ));
$dotenv->load();

// Log
$log = new Core\Tool\Logging();
$log->lfile(getenv('FILEPAHTLOG'));
$log->lwrite('Called..');

$downloader = new DataImportStockFileNavisionFTPDownloader\StockFileNavisionFTPDownloader();
$config = array(
    'ftpUser'         => getenv('FTPUSER'),
    'ftpPassword'     => getenv('FTPPASSWORD'),
    'ftpServer'       => getenv('FTPSERVER'),
    'ftpRemoteFolder' => getenv('FTPREMOTEFOLDER'), 
    'ftpLocalFolder'  => getenv('FTPLOCALFOLDER'), 
    'ftpLocalFile'    => getenv('FTPLOCALFILE'), 
    'delete'          => 0 ,
    'move'            => 0 ,
);

try { 
    $log->lwrite('Downloading..');
    fwrite(STDERR,'Downloading..');
    $fileName = $downloader->download($config);
    $log->lwrite("Downloaded: $fileName");
}catch( Exception $e) {
    // var_dump($e);
    $log = new Core\Tool\Logging();
    $log->lfile(getenv('FILEPAHTLOG'));
    // write message to the log file
    $log->lwrite(var_export($e, true));
    $log->lclose();;
}
$log->lwrite("Done.");
