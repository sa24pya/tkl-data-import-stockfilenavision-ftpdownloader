<?php
/**
 * src/StockFileNavisionFTPDownloader.php
 *
 * @author mrx <mma@shopall24.com>
 * 
 * @package DataImportStockFileNavisionFTPDownloader
 * @subpackage StockFileNavisionFTPDownloader
 * @version 1.0.0
 */
namespace DataImportStockFileNavisionFTPDownloader;

/**
 * Class StockFileNavisionFTPDownloader
 *
 *
 * @package DataImportStockFileNavisionFTPDownloader
 * @subpackage StockFileNavisionFTPDownloader
 * @version 1.0.0
 */
class StockFileNavisionFTPDownloader
{

    /**
     * Required fields
     *
     * @var array $config
     */
    private $config = array (
        'ftpUser', 
        'ftpPassword', 
        'ftpServer', 
        'ftpRemoteFolder', 
        'ftpLocalFolder',
        'ftpLocalFile'
    );

    /**
     * Config Validation
     *
     * @param array $passedConfig
     *
     * @return void
     * @throws Exception
     */
    private function configValidation(array $passedConfig)
    {
        foreach ($this->config as $key) {
            if (!array_key_exists($key, $passedConfig)
                    || empty( $passedConfig[$key] )) {
                throw new \Exception("Error: Missing parameter {$key}");
            }
        }
        return;
    }

    /**
     * Download, move, remove files via ftp connection
     *
     * @param array $config
     *
     * @return bool
     */
    public function download(array $config)
    {
        $ds = DIRECTORY_SEPARATOR;
        $this->configValidation($config);


        $conn_id = ftp_connect($config['ftpServer']) or die("Can't connect " . $config['ftpServer']);
        ftp_login($conn_id, $config['ftpUser'], $config['ftpPassword']);
        ftp_pasv($conn_id, true);

        $files = ftp_nlist($conn_id, $config['ftpRemoteFolder']);

        if ($files) {
            sort($files);
            $file = array_pop($files);
            $pwd = '/home/hosting/replicated/websites/site01';
            $pwd = '';
            $remoteFileName = $pwd . $file ;//substr($file, 1);

            $localDir = /* __DIR__ . */ $config['ftpLocalFolder'];
            $localFileName = realpath($localDir . $ds) . /* */  '/' . $config['ftpLocalFile'];

            // open some file to write to
            $handle = fopen($localFileName, 'w');

            ftp_fget($conn_id, $handle, $config['ftpRemoteFolder'] . $ds . basename($file) , FTP_BINARY);
            
            ftp_close($conn_id);
            fclose($handle);
            return  basename($file);
        }
        return 'No files found: ' . var_export($files,true);
    }

}
