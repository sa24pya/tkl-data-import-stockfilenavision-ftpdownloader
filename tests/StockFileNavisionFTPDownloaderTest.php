<?php
/**
 * src/StockFileNavisionFTPDownloader.php
 *
 * @author mrx <mma@shopall24.com>
 * 
 * @package DataImportStockFileNavisionFTPDownloader
 * @subpackage StockFileNavisionFTPDownloader
 * @version 1.0.0
 */
namespace DataImportStockFileNavisionFTPDownloader;

/**
 * Class StockFileNavisionFTPDownloaderTest
 *
 *
 * @package DataImportStockFileNavisionFTPDownloader
 * @subpackage StockFileNavisionFTPDownloader
 * @version 1.0.0
 */

class StockReaderTest extends \PHPUnit_Framework_TestCase
{

 	public function setUp() 
 	{
 		$dotenv = new \Dotenv\Dotenv( dirname( __DIR__ ));
        $dotenv->load();
 	}
    public function tearDown() {}


 	/**
     * Test class instance and namespace
     *
     */
    public function testStockReaderinstance()
    {
        $o = new StockFileNavisionFTPDownloader();
        $this->assertNotEmpty($o);
    }
 	/**
     * Test connection and download
     *
     */
    public function testDownload()
    {
    	$sfnftpd = new StockFileNavisionFTPDownloader();
        // Stable
        $this->assertTrue(!empty(
            $sfnftpd->download(
                [
                    'ftpUser'         => getenv('FTPUSER'),
                    'ftpPassword'     => getenv('FTPPASSWORD'),
                    'ftpServer'       => getenv('FTPSERVER'),
                    'ftpRemoteFolder' => getenv('FTPREMOTEFOLDER'), 
                    'ftpLocalFolder'  => getenv('FTPLOCALFOLDER'), 
                    'ftpLocalFile'    => getenv('FTPLOCALFILE'), 
                    'delete'          => 0 ,
                    'move'            => 0 ,
                ]
            )
        ));
    }



   
    /*
    public function testIncorrectParameter()
    {
        // Incorrect parameter
        try {
            $this->_ftp->download(
                [
                    'ftpUser'         => '5790002268342/it@shopall24.com',
                    'ftpPassword'     => 'TuVt9RZJ286HiRtz',
                    'ftpServer'       => 'gs1tradesync-',
                    'ftpRemoteFolder' => 'Outbox',
                    'ftpLocalFolder'  => 'ftpLocalFolder',
                    'delete'          => null
                ]
            );
        } catch (Exception $e) {
            return;
        }

        $this->fail("An expected exception for an incorrect parameter has not been rised");
    }

    /**
     * @expectedException
     */
    /*
    public function testMissingParameter()
    {
        try {
            // Missing Parameter
            $this->_ftp->download(
                [
                    'ftpUser'        => '5790002268342/it@shopall24.com',
                    'ftpPassword'    => 'TuVt9RZJ286HiRtz',
                    'ftpLocalFolder' => 'ftpLocalFolder',
                    'delete'         => null
                ]
            );
        } catch (Exception $e) {
            return;
        }

        $this->fail("An expected exception for a missing parameter has not been rised");

    }

    /**
     * @expectedException
     */
    /*
    public function testMove()
    {
        $this->assertTrue(
            $this->_ftp->download(
                [
                    'ftpUser'         => '5790002268342/it@shopall24.com',
                    'ftpPassword'     => 'TuVt9RZJ286HiRtz',
                    'ftpServer'       => 'gs1tradesync-uat.pro-sharp.hu',
                    'ftpRemoteFolder' => 'Outbox',
                    'ftpLocalFolder'  => 'ftpLocalFolder',
                    'move'            => 1

                ]
            )
        );
    }

    /**
     * @expectedException
     */
    /*
    public function testDelete()
    {
        $this->assertTrue(
            $this->_ftp->download(
                [
                    'ftpUser'         => '5790002268342/it@shopall24.com',
                    'ftpPassword'     => 'TuVt9RZJ286HiRtz',
                    'ftpServer'       => 'gs1tradesync-uat.pro-sharp.hu',
                    'ftpRemoteFolder' => 'Outbox',
                    'ftpLocalFolder'  => 'ftpLocalFolder',
                    'delete'          => 1
                ]
            )
        );
    }
    */
}
